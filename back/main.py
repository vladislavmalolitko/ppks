from flask import Flask
from flask_restful import Resource, Api, reqparse
from flask_cors import CORS
import pandas as pd

app = Flask(__name__)
api = Api(app)
CORS(app, support_credentials=True)

class Alarm(Resource):
    # get list of all alarms
    def get(self):
        #read data from database:D sorry for calling it as database
        data = pd.read_csv('data.txt')
        data = data.to_dict('records')
        return data

    def post(self):
        # request parser
        parser = reqparse.RequestParser()

        #params that should be parsed
        parser.add_argument('name')
        parser.add_argument('time')
        parser.add_argument('isActive')

        args = parser.parse_args()

        # check for required params
        if not (args['name'] and args['time'] and args['isActive'] != None):
            return {
                'success': False,
                'message': 'Parametrs missing'
            }

        # reading data from database
        rawData = pd.read_csv('data.txt')
        data = rawData.to_dict(orient='list')

        lastID = data['id'][-1]

        newAlarm = pd.DataFrame(
            data={
                'id': lastID + 1,
                'name': args['name'],
                'time': args['time'],
                'isActive': args['isActive']
            },
            index=[lastID]
        )

        # insert new alarm
        rawData = rawData.append(newAlarm)
        rawData.to_csv('data.txt', index=False)

        return {
            'success': True
        }

    # delete alarm
    def delete(self):
        parser = reqparse.RequestParser()

        parser.add_argument('id')

        args = parser.parse_args()

        if not args['id']:
            return {
                'success': False,
                'message': 'Id is required'
            }

        id = int(args['id'])

        rawData = pd.read_csv('data.txt')
        data = rawData.to_dict('index')

        if len(data) + 1 <= id:
            return {
                'success': False,
                'message': 'No such id'
            }

        del data[id - 1]

        rawData = pd.DataFrame.from_dict(data, orient='index')
        rawData.to_csv('data.txt', index=False)

        return {
            'success': True
        }

    # change alarm
    def put(self):
        parser = reqparse.RequestParser()

        parser.add_argument('id')
        parser.add_argument('name')
        parser.add_argument('time')
        parser.add_argument('isActive')

        args = parser.parse_args()

        if not (args['id'] and args['name'] and args['time'] and args['isActive'] != None):
            return {
                'success': False,
                'message': 'Parametrs missing'
            }

        rawData = pd.read_csv('data.txt')
        data = rawData.to_dict('index')

        id = int(args['id'])
        if len(data) + 1 <= id:
            return {
                'success': False,
                'message': 'No such id'
            }

        data[id - 1]['name'] = args['name']
        data[id - 1]['time'] = args['time']
        data[id - 1]['isActive'] = args['isActive']

        rawData = pd.DataFrame.from_dict(data, orient='index')
        rawData.to_csv('data.txt', index=False)

        return {
            'success': True
        }

api.add_resource(Alarm, '/')

if __name__ == '__main__':
    # API URL : http://127.0.0.1:5000
    app.run(debug=True)